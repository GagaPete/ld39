# Theme: Running out of power
 * Losing power supply
    * of life support systems
    * of weaponery
 * A king losing his power
 * Visual novel/"Chat adventure" with time limit (solve a problem until power of the phone runs out)
 * A weapon get weaker each use
 * Endless runner like (you're data and need to get out of system before power
   runs out).
 * Matrix
 * Robot absorbing enemies power?
Platformer or Top-Down?






# Unlucky Jeff / Jess
An astronaut who often gets into the situation that power supply runs down for
the stations he's on. To survive he needs to seal the stations doors and lock
the others on the station out and let them die poorly.





# Final design
You're a drone with a broken battery in an abandoned science or production
facility. This facility is secured by other (proper working) drones and turrets.
You need to recharge, either by reaching charging stations or by taking over
other drones and turrets.


Your wireless charger broke down
Reach the repair station
Get power from turrets and other drones
