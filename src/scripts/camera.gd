extends Camera2D

export(NodePath) var follow_path setget set_follow_path
var follow_node
var screen = Vector2()

signal screen_changed

func set_follow_path(new_follow_path):
	follow_path = new_follow_path
	if follow_path != null:
		set_fixed_process(true)
		update_screen()
	else:
		set_fixed_process(false)

func update_screen():
	if follow_node == null:
		if has_node(follow_path):
			follow_node = get_node(follow_path)
		else:
			return
	var new_screen = Vector2()
	var camera_pos = Vector2()
	var follow_pos = follow_node.get_global_pos()
	new_screen.x = floor(follow_pos.x / 1920)
	new_screen.y = floor(follow_pos.y / 1080)
	camera_pos.x = new_screen.x * 1920
	camera_pos.y = new_screen.y * 1080
	set_global_pos(camera_pos)
	
	if screen != new_screen:
		emit_signal("screen_changed", new_screen)
		screen = new_screen

func _fixed_process(delta):
	update_screen()