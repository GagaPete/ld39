extends Area2D

const CHARGING_SPEED = 50
var charging = []

func _ready():
	set_fixed_process(true)

func _fixed_process(delta):
	for chargeable in charging:
		chargeable.charge(CHARGING_SPEED * delta)
	

func _on_charger_body_enter(body):
	if body.is_in_group("chargeable"):
		charging.append(body)

func _on_charger_body_exit(body):
	if body.is_in_group("chargeable"):
		charging.erase(body)
