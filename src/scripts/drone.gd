tool
extends "res://scripts/robot.gd"

export(int, "Left", "Right") var facing = 1

onready var ray_left = get_node("ray_left")
onready var ray_right = get_node("ray_right")
onready var ray_ground = get_node("ground")
onready var ray_forward = get_node("hook/ray_forward")
onready var ray_shoot = get_node("hook/ray_shoot")

func _ready():
	set_fixed_process(false)
	ray_right.add_exception(self)
	ray_forward.add_exception(self)
	if facing == 0:
		set_direction(-1)
	else:
		set_direction(1)

func _fixed_process(delta):
	if power <= 0:
		return
	if ray_forward.is_colliding():
		set_direction(-direction)
	elif direction == -1 and not ray_left.is_colliding() and ray_ground.is_colliding():
		set_direction(-direction)
	elif direction == 1 and not ray_right.is_colliding() and ray_ground.is_colliding():
		set_direction(-direction)
	if ray_shoot.is_colliding() and ray_shoot.get_collider().is_in_group("player") and ray_shoot.get_collider().power > 0:
		shoot()