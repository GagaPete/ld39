extends Node

var restart_was_pressed = false
var game_started = false

func _ready():
	set_process(true)

func _process(delta):
	var restart_is_pressed = Input.is_action_pressed("game_restart")
	if restart_is_pressed and not restart_was_pressed:
		if get_node("/root/game/player").power <= 0:
			get_tree().reload_current_scene()
	
	restart_was_pressed = restart_is_pressed

func _on_start_body_enter( body ):
	if body.is_in_group("player") and body.power <= 0 and not game_started:
		get_node("animation").play("intro")
		game_started = true
