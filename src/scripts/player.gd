tool
extends "res://scripts/robot.gd"

const TRANSFER_POWER_SPEED = 35
onready var ray_front = get_node("hook/ray_front")

func _ready():
	ray_front.add_exception(self)
	
func _fixed_process(delta):
	if power <= 0:
		return
	
	var direction = 0
	if Input.is_action_pressed("game_left") and controllable:
		direction -= 1
	if Input.is_action_pressed("game_right") and controllable:
		direction += 1
	set_direction(direction)
	boosting = Input.is_action_pressed("game_up") and controllable
	# We can transfer power from other chargeables
	if Input.is_action_pressed("game_use") and ray_front.is_colliding() and ray_front.get_collider().is_in_group("power_source"):
		var source = ray_front.get_collider()
		var power = min(source.power, TRANSFER_POWER_SPEED * delta)
		charge(power)
		if self.power < 100:
			source.power -= power
		else:
			source.power = 0