extends TextureProgress

export(NodePath) var player_path = null
onready var player = get_node(player_path)

func _ready():
	set_fixed_process(true)

func _fixed_process(delta):
	set_value(player.power)
