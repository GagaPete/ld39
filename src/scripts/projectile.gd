extends Sprite

var shooter = null
var direction = Vector2(1, 0)

func set_direction(direction):
	direction = direction.normalized()
	self.direction = direction
	set_global_rot(direction.angle() - PI * 0.5)

func _ready():
	set_fixed_process(true)

func _fixed_process(delta):
	set_pos(get_pos() + (direction * 500 * delta))

func _on_area_body_enter(body):
	if body == shooter:
		return
	if "power" in body:
		body.power = -1
	queue_free()