extends RigidBody2D

enum Direction {
	None = 0,
	Left = -1,
	Right = 1
}
var direction = Direction.None
var boosting = false
onready var cooldown_timer = get_node("cooldown_timer")
var can_shoot = true
var first_contact = true
var first_disabled = true
export(bool) var controllable = false

onready var ground = get_node("ground")
onready var hook = get_node("hook")
onready var samples = get_node("sample_player")
export(int, 0, 100) var power = 100
export(bool) var consume_power = false
export(PackedScene) var projectile_scene = null
export(Texture) var dead_texture = null
var alive_texture = null

const VERTICAL_POWER_CONSUMPTION = 8
const UPWARDS_POWER_CONSUMPTION = 11

const VERTICAL_SPEED = 200
const UPWARDS_SPEED = 180

func charge(power):
	var powerbefore = self.power
	self.power += power
	if self.power > 100:
		self.power = 100
	if self.power > 0:
		set_mode(MODE_CHARACTER)
		set_rotd(0)
		get_node("hook/sprite").set_texture(alive_texture)
		if powerbefore <= 0:
			samples.play("startup")

func shoot():
	if not can_shoot:
		return
	can_shoot = false
	cooldown_timer.start()
	var projectile = projectile_scene.instance()
	projectile.shooter = self
	projectile.set_direction(Vector2(direction, 0))
	get_parent().add_child(projectile)
	get_parent().move_child(projectile, 0)
	projectile.set_global_pos(get_global_pos())
	samples.play("shoot")

func set_direction(direction):
	self.direction = direction
	if direction == Direction.Left:
		hook.set_scale(Vector2(-1, 1))
	if direction == Direction.Right:
		hook.set_scale(Vector2(1, 1))

func _ready():
	set_fixed_process(true)
	ground.add_exception(self)
	alive_texture = get_node("hook/sprite").get_texture()
	if dead_texture == null:
		dead_texture = alive_texture
	if power == 0:
		first_contact = false
		first_disabled = false

func _integrate_forces(state):
	var velocity = state.get_linear_velocity()
	if power > 0:
		velocity.x = 0
		if direction == Direction.Left:
			velocity.x -= VERTICAL_SPEED
			hook.set_scale(Vector2(-1, 1))
			if consume_power:
				power -= VERTICAL_POWER_CONSUMPTION * state.get_step()
			
		if direction == Direction.Right:
			velocity.x += VERTICAL_SPEED
			hook.set_scale(Vector2(1, 1))
			if consume_power:
				power -= VERTICAL_POWER_CONSUMPTION * state.get_step()
		
		if boosting:
			velocity.y = -UPWARDS_SPEED
			if consume_power:
				power -= UPWARDS_POWER_CONSUMPTION * state.get_step()
	state.set_linear_velocity(velocity)

func _fixed_process(delta):
	if power <= 0:
		controllable = false
		if first_disabled and samples:
			samples.play("deactivate")
			first_disabled = false
		set_mode(MODE_RIGID)
		get_node("hook/sprite").set_texture(dead_texture)
		set_contact_monitor(true)
		set_max_contacts_reported(1)
		if first_contact and get_colliding_bodies().size() > 0 and samples:
			samples.play("hit_ground")
			first_contact = false
		return
	elif get_rotd() != 0:
		set_rotd(0)
	
	if ground.is_colliding():
		var dist = ground.get_collision_point().y - ground.get_global_pos().y
		if dist < 40:
			apply_impulse(Vector2(), Vector2(0, -8 * (1 - (dist / 50))))

func _on_cooldown_timer_timeout():
	can_shoot = true
