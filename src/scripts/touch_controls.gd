extends Node

var actions = {}
var press_count = 0

func _ready():
	set_process_input(true)

func _input(event):
	if event.type == InputEvent.SCREEN_TOUCH:
		if event.pressed:
			var action
			var position = event.pos / get_viewport().get_visible_rect().size
			if position.x < 0.33:
				action = "game_left"
			elif position.x >= 0.67:
				action = "game_right"
			elif position.y < 0.5:
				action = "game_use"
			else:
				action = "game_up"
			Input.action_press(action)
			actions[event.index] = action
			
			# Handle restarts
			if press_count == 0:
				Input.action_press("game_restart")
				print("Game Restart Press")
			press_count = press_count + 1
			
		else:
			if event.index in actions and actions[event.index] != null:
				Input.action_release(actions[event.index])
				actions[event.index] = null
				
				# Handle restarts
				press_count = press_count - 1
				if press_count == 0:
					Input.action_release("game_restart")
					print("Game Restart Release")