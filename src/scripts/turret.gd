tool
extends StaticBody2D

export(int, "Left", "Right") var facing = 1 setget set_facing
export(int, 0, 90) var angle = 0 setget set_angle
export(int, 0, 100) var power = 100 setget set_power

const projectile_scene = preload("res://objects/drone_projectile.tscn")
const alive_texture = preload("res://images/drone_body.png")
const dead_texture = preload("res://images/drone_dead.png")

onready var turret = get_node("head")
onready var mount = get_node("mount")
onready var cooldown_timer = get_node("cooldown_timer")
onready var ray_sight = get_node("head/ray_sight")
onready var samples = get_node("sample_player")
onready var tween = get_node("tween")
var can_shoot = true

func update_orientation():
	if not is_inside_tree():
		return
	var turret = get_node("head")
	var mount = get_node("mount")
	var ray_sight = get_node("head/ray_sight")
	var pos = ray_sight.get_pos()
	pos.x = abs(pos.x)
	var cast = Vector2(1920, 0)
	cast.x = abs(cast.x)
	if facing == 0:
		turret.set_flip_h(true)
		turret.set_rotd(-angle)
		mount.set_flip_h(true)
		ray_sight.set_pos(pos * Vector2(-1, 1))
		ray_sight.set_cast_to(cast * Vector2(-1, 1))
	else:
		turret.set_flip_h(false)
		turret.set_rotd(angle)
		mount.set_flip_h(false)
		ray_sight.set_pos(pos)
		ray_sight.set_cast_to(cast)

func update_visuals():
	if not is_inside_tree():
		return
	var turret = get_node("head")
	if power > 0:
		turret.set_texture(alive_texture)
	else:
		turret.set_texture(dead_texture)

func shoot():
	if not can_shoot:
		return
	can_shoot = false
	cooldown_timer.start()
	var projectile = projectile_scene.instance()
	projectile.shooter = self
	var angle = turret.get_global_rot()
	if facing == 0:
		angle += PI
	projectile.set_direction(Vector2(1, 0).rotated(angle))
	get_parent().add_child(projectile)
	get_parent().move_child(projectile, 0)
	projectile.set_global_pos(turret.get_global_pos())
	samples.play("shoot")

func set_facing(new):
	facing = clamp(new, 0, 1)
	update_orientation()

func set_angle(new):
	angle = clamp(new, 0, 90)
	update_orientation()

func set_power(new):
	if power > 0 and new <= 0:
		samples.play("deactivate")
	power = clamp(new, 0, 100)
	update_visuals()

func _ready():
	update_orientation()
	update_visuals()
	
func _fixed_process(delta):
	if power == 0:
		return
	var players = get_tree().get_nodes_in_group("player")
	var angle = turret.get_global_pos().angle_to_point(players[0].get_global_pos()) - get_global_rot()
	if facing == 0 and angle > 0 and angle < PI * 0.5:
		angle = angle - PI * 0.5
	elif facing == 1 and angle > PI * -0.5 and angle < 0:
		angle = angle + PI * 0.5
	else:
		angle = turret.get_rot()
	tween.interpolate_method(turret, "set_rot", turret.get_rot(), angle, 0.2, Tween.TRANS_LINEAR, Tween.EASE_OUT_IN)
	tween.start()
	if ray_sight.is_colliding() and ray_sight.get_collider().is_in_group("player") and ray_sight.get_collider().power > 0:
		shoot()

func _on_cooldown_timer_timeout():
	can_shoot = true
